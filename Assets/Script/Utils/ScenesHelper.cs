﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesHelper : MonoBehaviour {
    public enum Scenes { LOGIN, REGISTER, MAINMENU, GAMEPLAY, STATISTIC, LEADERBOARD, HOME, INIT }
    public Scenes TargetScene;

    public void MoveTo() {
        SceneManager.LoadScene(Config.Scenes[TargetScene.ToString()]);
    }

    public void Quit() {
        Application.Quit();
    }

    public void Logout() {
        User.Clear();
        SceneManager.LoadScene(Config.Scenes["LOGIN"]);
    }
}
