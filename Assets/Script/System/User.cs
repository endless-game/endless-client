﻿public class User {
    public static string name;
    public static string username;
    public static string token;

    public static void Inject(string name, string username, string token) {
        User.name = name;
        User.username = username;
        User.token = token;
    }

    public static void Clear() {
        User.name = "";
        User.username = "";
        User.token = "";
    }
}
