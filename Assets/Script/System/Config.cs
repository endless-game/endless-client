﻿using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour {
    public static bool HasInited = false;

    //public static string API_URL = "http://infinite-harbor-57581.herokuapp.com";
    public static string API_URL = "http://localhost:3000";

    public enum AllMode { PRODUCTION, DEVELOPMENT };
    public static AllMode Mode = AllMode.PRODUCTION;

    public static string NotAuthenticationRedirectScene = "Login";

    public static Dictionary<string, string> Scenes = new Dictionary<string, string>() {
        { "MAINMENU", "MainMenu" },
        { "GAMEPLAY", "Gameplay" },
        { "STATISTIC", "Statistic" },
        { "LEADERBOARD", "LeaderBoard" },

        { "HOME", "Home" },
        { "LOGIN", "Login" },
        { "REGISTER", "Register" },
        { "SPLASHSCREEN", "SplashScreen" },
        { "INIT", "Init" },
        { "MAINTENANCE", "Maintenance" },
    };

    public static void Init() {
        if (HasInited)
            return;

        if(Mode == AllMode.PRODUCTION) {

        }
        else if(Mode == AllMode.DEVELOPMENT) {
            User.Inject("Aezakmi", "aezakmi", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFlemFrbWkiLCJuYW1lIjoiQWV6YWttaSIsImlhdCI6MTU0MDc4ODkwMSwiZXhwIjoxNTQwODc1MzAxfQ.MQ40cru1vXscDftRxjarbMMsy4iaYTNpiGEswj0jgVM");
        }

        HasInited = true;
    }
}
