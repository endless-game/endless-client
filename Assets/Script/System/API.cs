﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class API {
    /* Authentication API */
    public static readonly string LOGIN = "/api/auth/login";
    public static readonly string REGISTER = "/api/auth/register";

    /* Game API */
    public static readonly string SAVE_SCORE = "/api/game/save-score";
    public static readonly string STATS = "/api/game/stats";
    public static readonly string LEADERBOARD = "/api/game/leaderboard";

    /* System API */
    public static readonly string STATUS = "/api/system/status";
    public static readonly string VERSION = "/api/system/version";

    public static IEnumerator Request(string url, Func<WWWForm, Dictionary<string, string>> setup, Func<WWW, int> callbackSuccess, Func<WWW, int> callbackError) {
        WWWForm form = new WWWForm();

        // Get headers
        Dictionary<string, string> headers = setup(form);

        WWW www = new WWW(Config.API_URL + url, form.data.Length > 0 ? form.data : null, headers);
        yield return www;

        if (!string.IsNullOrEmpty(www.error)) {
            callbackError(www);
        }
        else {
            callbackSuccess(www);
        }
    }
}
