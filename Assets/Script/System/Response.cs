﻿using System;

public class Response {
    [Serializable]
    public class MESSAGE {
        public string message;
    }

    [Serializable]
    public class LOGIN_SUCCESS {
        public string username;
        public string name;
        public string token;
    }

    [Serializable]
    public class LEADERBOARD {
        public string username;
        public int highscore;
        public int timestamp;
    }

    [Serializable]
    public class Score {
        public int timestamp;
        public int value;
    }

    [Serializable]
    public class STATS {
        public string username;
        public Score highscore;
        public Score[] scores;
        public float average;
    }

    [Serializable]
    public class Dependencies {
        public string assetbundles;
        public string[] images;
        public string[] audios;
    }

    [Serializable]
    public class VERSION {
        public string version;
        public string release;
        public Dependencies dependencies;
    }

    [Serializable]
    public class STATUS {
        public string status;
        public string version;
    }
}
