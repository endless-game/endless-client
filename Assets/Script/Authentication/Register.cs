﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Register : MonoBehaviour {
    public InputField InputName;
    public InputField InputUsername;
    public InputField InputPassword;
    public Text TextStatus;

	public void ButtonRegist () {
        StartCoroutine(API.Request(API.REGISTER, (WWWForm form) => {
            form.AddField("name", InputName.text);
            form.AddField("username", InputUsername.text);
            form.AddField("password", InputPassword.text);

            TextStatus.text = "Waiting to Register...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Register Success");
            TextStatus.text = "Register Success";

            SceneManager.LoadScene(Config.Scenes["LOGIN"]);

            return 0;
        }, (WWW www) => {
            Debug.Log("Register Failed " + www.text);

            Response.MESSAGE obj = JsonUtility.FromJson<Response.MESSAGE>(www.text);

            TextStatus.text = obj.message;

            return 1;
        }));
	}
}
