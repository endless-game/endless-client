﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Init : MonoBehaviour {
    public Text TextStatus;

	void Start () {
        CheckStatus();
	}

    void CheckStatus() {
        // Get current version from server
        StartCoroutine(API.Request(API.STATUS, (WWWForm form) => {
            TextStatus.text = "Checking current status from server...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log(www.text);
            TextStatus.text = "Loading...";

            // Parsing to object
            Response.STATUS server = JsonUtility.FromJson<Response.STATUS>(www.text);

            // Check if server status is maintenance
            if(server.status.Equals("maintenance")) {
                SceneManager.LoadScene(Config.Scenes["MAINTENANCE"]);
                return 0;
            }

            // Check game version
            CheckVersion();

            return 0;
        }, (WWW www) => {
            Debug.Log("Version Failed " + www.text);

            TextStatus.text = "Failed checking version from server...";

            return 1;
        }));
    }

    void CheckVersion() {
        // Get current version from server
        StartCoroutine(API.Request(API.VERSION, (WWWForm form) => {
            TextStatus.text = "Checking current version from server...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log(www.text);
            TextStatus.text = "Downloading Dependencies...";

            // Parsing to object
            Response.VERSION ver = JsonUtility.FromJson<Response.VERSION>(www.text);

            // If current version not same with local version then setup the local game
            SetupDependencies(ver);

            return 0;
        }, (WWW www) => {
            Debug.Log("Version Failed " + www.text);

            TextStatus.text = "Failed checking version from server...";

            return 1;
        }));
    }

    // Method to download and use dependencies
	void SetupDependencies(Response.VERSION ver) {
        // Download assetbundles
        StartCoroutine(DownloadAssetbundles(ver.dependencies.assetbundles));
    }

    IEnumerator DownloadAssetbundles(string asset) {
        WWW www = WWW.LoadFromCacheOrDownload(asset, 1);
        yield return www;

        if (www.error != null) {
            Debug.Log(www.error);
        }
        else {
            TextStatus.text = "Loading asset success...";

            // Save to assethelper bundle
            AssetHelper.unity = www.assetBundle;

            // Change scene
            SceneManager.LoadScene(Config.Scenes["HOME"]);
        }
    }
}
