﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Statistic : MonoBehaviour {
    public GameObject rowStatisticPrefab;
    public Transform statisticParent;
    public Text textStatus;
    public Text textHighscore;
    public Text textAverage;

    void Awake() {
        // Init config
        Config.Init();
        
        // Check credentials
        if (!Authentication.IsLogin())
            SceneManager.LoadScene(Config.NotAuthenticationRedirectScene);
    }

    // Use this for initialization
    void Start() {
        StartCoroutine(API.Request(API.STATS, (WWWForm form) => {
            textStatus.text = "Fetching data from server...";
            return new Dictionary<string, string> { { "Authorization", User.token } };
        }, (WWW www) => {
            textStatus.text = "";

            // Load statistic to scrollview
            LoadStatistic(www.text);

            return 0;
        }, (WWW www) => {
            textStatus.text = "Oopss failed fetching data...";

            Debug.Log(www.text);
            return 1;
        }));
    }

    void LoadStatistic(string text) {
        Debug.Log(text);
        Response.STATS stats = JsonUtility.FromJson<Response.STATS>(text);

        textHighscore.text = "Highscore: " + stats.highscore.value;
        textAverage.text = "Average: " + stats.average + "";

        for (int i = 0; i < stats.scores.Length; i++) {
            rowStatisticPrefab.GetComponentsInChildren<Text>()[0].text = stats.scores[i].value + "";
            rowStatisticPrefab.GetComponentsInChildren<Text>()[1].text = new DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)
                .ToLocalTime().AddSeconds(stats.scores[i].timestamp) + "";

            Instantiate(rowStatisticPrefab, statisticParent);
        }
    }
}
