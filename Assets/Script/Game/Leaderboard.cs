﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour {
    public GameObject rowLeaderboardPrefab;
    public Transform leaderboardParent;
    public Text textStatus;

    void Awake() {
        // Init config
        Config.Init();

        // Check credentials
        if (!Authentication.IsLogin())
            SceneManager.LoadScene(Config.NotAuthenticationRedirectScene);
    }

    // Use this for initialization
    void Start () {
        StartCoroutine(API.Request(API.LEADERBOARD, (WWWForm form) => {
            textStatus.text = "Fetching data from server...";
            return new Dictionary<string, string> { { "Authorization", User.token } };
        }, (WWW www) => {
            textStatus.text = "";

            // Load leaderboard to scrollview
            LoadLeaderboard(www.text);

            return 0;
        }, (WWW www) => {
            textStatus.text = "Oopss failed fetching data...";

            Debug.Log(www.text);
            return 1;
        }));
	}

    void LoadLeaderboard(string text) {
        Debug.Log(text);
        Response.LEADERBOARD[] rows = JsonHelper.FromJSON<Response.LEADERBOARD>(text);

        for (int i = 0; i < rows.Length; i++) {
            rowLeaderboardPrefab.GetComponentsInChildren<Text>()[0].text = rows[i].username + " - " + rows[i].highscore;
            rowLeaderboardPrefab.GetComponentsInChildren<Text>()[1].text = new DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)
                .ToLocalTime().AddSeconds(rows[i].timestamp) + "";

            Instantiate(rowLeaderboardPrefab, leaderboardParent);
        }
    }
}
