﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiManager : MonoBehaviour {

	public Text scoreText;
	bool gameOver;
	int score;

	// Use this for initialization
	void Start () {
		gameOver = false;
		Data.score = 0;
		InvokeRepeating("scoreUpdate", 1.0f,  0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score: " + Data.score;
	}

	void scoreUpdate(){
		if(gameOver == false){
		    Data.score += 1;
		}
	}

	public void gameOverActivated(){
		gameOver = true;
	}
}
