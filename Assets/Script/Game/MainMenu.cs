﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    void Awake() {
        // Init config
        Config.Init();

        // Check credentials
        if (!Authentication.IsLogin())
            SceneManager.LoadScene(Config.NotAuthenticationRedirectScene);
    }

}