﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text txtStatus;

    bool back = false;
    float timer = 0;

    // Update is called once per frame
    void Update()
    {
        if (!back)
            return;

        //timer += Time.deltaTime;
        if (timer > 5 || Input.GetKey(KeyCode.Escape)) {
            Data.score = 0;
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }

    void Start() {
        StartCoroutine(API.Request(API.SAVE_SCORE, (WWWForm form) => {
            form.AddField("score", Data.score);

            //txtStatus.text = "Waiting Save to Server...";

            return new Dictionary<string, string> { { "Authorization", User.token } };
        }, (WWW www) => {
            Debug.Log("Save Sukses");
            txtStatus.text = "Save Skor Success";

            back = true;

            return 0;
        }, (WWW www) => {
            Debug.Log("Save Failed " + www.text);

            txtStatus.text = "Save Failed";
            back = true;

            return 1;
        }));
    }
}