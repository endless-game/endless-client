﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour {
    void Awake() {
        var prefab = AssetHelper.unity.LoadAsset<GameObject>("Player");
        
        Instantiate(prefab);
    }
}
