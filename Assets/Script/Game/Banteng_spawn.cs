﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banteng_spawn : MonoBehaviour {

	public GameObject banteng;
	public float maxPos = 5.1f;
	public float delayTimer = 1f;
	float timer;

	// Use this for initialization
	void Start () {		
		timer = delayTimer;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Vector3 bantengPos = new Vector3(Random.Range(-5.1f, 5.1f), transform.position.y, transform.position.z);

            Instantiate(banteng, bantengPos, transform.rotation);
			timer = delayTimer;
        }
	}
}
