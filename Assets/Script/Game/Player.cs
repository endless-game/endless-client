﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public float coboySpeed;
    public float maxPos = 3.7f;
    Vector3 position;
    public uiManager ui;

    void Start()
    {
        //ui = GetComponent<uiManager>();
        position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        position.x += Input.GetAxis("Horizontal") * coboySpeed * Time.deltaTime;
        position.x = Mathf.Clamp(position.x, -3.7f, 3.7f);
        transform.position = position;    
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Enemy")
        {
            //Destroy(gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }

    void Awake(){

    }
}